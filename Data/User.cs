﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Avaukro.Data;

public class User
{
	[Key]
	public required string UserName { get; set; }

	[Required]
	public required string PasswordHash { get; set; }

	[Required]
	public required string FirstName { get; set; }

	[Required]
	public required string LastName { get; set; }

	public ICollection<Offer> Offers = new List<Offer>();
}
