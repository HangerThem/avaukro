﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Avalonia.Controls.Chrome;

namespace Avaukro.Data;

public class Offer
{
	public Guid Id { get; set; }

	[Required]
	public required string Title { get; set; }

	[Required]
	public required string Description { get; set; }

	[Required]
	public required User Seller { get; set; }

	[Range(0, int.MaxValue)]
	public required int Price { get; set; }

	[MinLength(1)]
	public required ICollection<Category> Categories = new List<Category>();

	[Required]
	public required DateTime DateListed { get; set; }

	public DateTime? DateSold { get; set; }
}
