﻿using Avaukro.Utils;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Splat;

namespace Avaukro.Data;

// TODO
public class ApplicationDbContext : DbContext //IdentityDbContext<ApplicationUser, IdentityRole, string>
{
	public DbSet<User> Users { get; set; }
	public DbSet<Offer> Offers { get; set; }
	public DbSet<Category> Categories { get; set; }

	public static void Migrate()
	{
		Locator.Current.GetRequiredService<ApplicationDbContext>().Database.Migrate();
	}

	protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
	{
		if (!optionsBuilder.IsConfigured)
		{
			optionsBuilder.UseSqlServer("Server=(localdb)\\mssqllocaldb;Database=Avaukro;Trusted_Connection=True;MultipleActiveResultSets=true");
		}
	}

	protected override void OnModelCreating(ModelBuilder builder)
	{
		// Method intentionally left empty.
	}
}
