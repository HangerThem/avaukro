﻿using System;
using Splat;

namespace Avaukro.Utils;

public static class ReadonlyDependencyResolverExtentions
{
	public static TService GetRequiredService<TService>(this IReadonlyDependencyResolver resolver)
		=> resolver.GetService<TService>()
			?? throw new InvalidOperationException($"Failed to resolve object of type {typeof(TService)}");
}
