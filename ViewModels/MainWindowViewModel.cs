﻿using Avaukro.Data;
using System;
using Avaukro.Views;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure.Internal;
using ReactiveUI;

namespace Avaukro.ViewModels;

public class MainWindowViewModel : ViewModelBase
{
	private ViewModelBase content;

	public MainWindowViewModel()
	{
		Content = HomeView = new HomeViewModel();
	}
	
	public ViewModelBase Content
	{
		get => content;
		private set => this.RaiseAndSetIfChanged(ref content, value);
	}

	public HomeViewModel HomeView { get; }

	public void MainViewModel()
	{
		Content = new HomeViewModel();
	}

	public void Login()
	{
		Content = new LoginViewModel();
	}
}
