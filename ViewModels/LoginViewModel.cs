using System;
using Avalonia.Interactivity;
using Avaukro.Models;
using Avaukro.Services;
using Avaukro.Utils;
using Microsoft.Extensions.DependencyInjection;
using Splat;

namespace Avaukro.ViewModels;

public class LoginViewModel : ViewModelBase
{
	private readonly LoginModel model = new();
	private readonly UserManager userManager;

	public string UserName => model.UserName;
	public string Password => model.Password;

	public LoginViewModel()
	{
		userManager = Locator.Current.GetRequiredService<UserManager>();
	}

	public async void Login(string userName, string password)
	{
		if (userName is null || password is null)
		{
			//errorMessage.Content = "Both username and password are required!";
			return;
		}

		try
		{
			await userManager.LoginAsync(userName, password).ConfigureAwait(true);
		}
		catch (InvalidLoginException ex)
		{
			//errorMessage.Content = ex.Message;
		}

		// TODO: jump to main page
	}

	public async void Register(object source, RoutedEventArgs args)
	{
		//var window = MainWindow
	}
}
