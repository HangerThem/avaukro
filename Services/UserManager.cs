﻿using System;
using System.Threading.Tasks;
using Avaukro.Data;
using Avaukro.Utils;
using Microsoft.EntityFrameworkCore;

namespace Avaukro.Services;

public class UserManager
{
	private readonly ApplicationDbContext dbContext;

	public UserManager(ApplicationDbContext dbContext)
	{
		this.dbContext = dbContext;
	}

	public async Task<User> LoginAsync(string userName, string password)
	{
		var user = await dbContext.Users.FirstOrDefaultAsync(x => x.UserName == userName)
			.ConfigureAwait(false);

		if (user is null || password.GetSHA256Hash() != user.PasswordHash)
		{
			throw new InvalidLoginException("Invalid username and/or password.");
		}

		return user;
	}

	public async Task<User> RegisterAsync(string userName, string firstName, string lastName, string password)
	{
		var user = await dbContext.Users.FirstOrDefaultAsync(x => x.UserName == userName)
			.ConfigureAwait(false);

		if (user is not null)
		{
			throw new InvalidLoginException("User is already registered.");
		}

		user = new User
		{
			UserName = userName,
			FirstName = firstName,
			LastName = lastName,
			PasswordHash = password.GetSHA256Hash(),
		};

		await dbContext.Users.AddAsync(user).ConfigureAwait(false);
		await dbContext.SaveChangesAsync().ConfigureAwait(false);

		return user;
	}
}

public class InvalidLoginException : Exception
{
	public InvalidLoginException()
	{
	}

	public InvalidLoginException(string? message)
		: base(message)
	{
	}

	public InvalidLoginException(string? message, Exception? innerException)
		: base(message, innerException)
	{
	}
}
